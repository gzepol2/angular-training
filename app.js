(function() {
  var app = angular.module('comicSite', ['ngRoute','ngStorage','checklist-model']);
  app.config(function($routeProvider) {
    $routeProvider
      .when('/', {
        controller: 'MainController',
        controllerAs:'site',
        templateUrl: 'views/home.html'
      })
      .when('/view/:id', {
        controller: 'DetailController',
        controllerAs:'detail',
        templateUrl: 'views/detail-comic.html'
      })
      .when('/login', {
        controller: 'LoginController',
        controllerAs: 'login',
        templateUrl: 'views/login.html'
      })
      .when('/admin', {
        controller: 'AdminController',
        controllerAs: 'admin',
        templateUrl: 'views/admin.html'
      });
  });

})();
