var app = angular.module('comicSite');

app.filter('mycomics', function (LoginService) {
    return function (comics, profile) {
        var out=[];
        if(!profile)
          return comics;
          
        angular.forEach(comics, function (value, key) {
        		//console.log("inside",value.genre,"---",genres[i])
        		if (value.borrowed == LoginService.getUser()) {
                	out.push(value);
            	}
        });
        if(out.length==0)
        	return comics;
        return out;
    };
})
.filter('bygenre', function () {
    return function (comics, genres) {
        var out=[];
        angular.forEach(comics, function (value, key) {
        	for (var i = genres.length - 1; i >= 0; i--) {
        		//console.log("inside",value.genre,"---",genres[i])
        		if (value.genre == genres[i]) {
                	out.push(value);
            	}
        	}
        });
        if(out.length==0)
        	return comics;
        return out;
    };
})
.filter('bycharacter', function () {
    return function (comics, characters) {
        var out=[];
        angular.forEach(comics, function (value, key) {
        	for (var i = characters.length - 1; i >= 0; i--) {
        		//console.log("inside",value.genre,"---",characters[i])
        		if (value.characters.indexOf(characters[i]) != -1) {
                	out.push(value);
                  break;//prevent duplictes items
            	}
        	}
        });
        if(out.length==0)
        	return comics;
        return out;
    };
})
.filter('bynews', function () {
    return function (comics, news) {
        var out=[];
        if(!news)
          return comics;

        angular.forEach(comics, function (value, key) {
        		if (value.date > 1995) {
                	out.push(value);
            	}
        });
        return out;
    };
})
.filter('stars', function() {
  return function(input, total) {
    total =Math.round(parseFloat(total));

    for (var i=0; i<total; i++) {
      input.push(i);
    }

    return input;
  };
});
