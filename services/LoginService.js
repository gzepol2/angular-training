var app = angular.module('comicSite');

app.service('LoginService',function($localStorage){
  var response={}
  var users=[];

  this.init=function(){
    users=$localStorage.users;
    if(!users){
      $localStorage.users=[{username:"admin",password:"pass"}];
    }
  };

  this.login=function(user){
    users=$localStorage.users;
    if(users){
    //  console.log(users);
      for (var i = 0; i < users.length; i++) {
        if(users[i].username==user.username && users[i].password==user.password){
          response.success=true;
          $localStorage.currentUser=users[i];
          return response;
        }
      }
      response.success=false;
      response.message="username or password incorrects";
    }else{
      //$localStorage.users=[{user:"admin",password:"pass"}];
        response.success=false;
        response.message="Error in local storage";
    }
    return response;
  }

  this.register=function(user){
    users=$localStorage.users;
    var newUser={
      username:user.username,
      password:user.password
    }

    for (var i = 0; i < users.length; i++) {
      if(users[i].username==newUser.username){
        response.success=false;
        response.message="The user already exist";
        return response;
      }
    }

    users.push(newUser);
    $localStorage.users=users;
    $localStorage.currentUser=newUser;
    response.success=true;
    return response;
  }

  this.checkAuth=function(){
    return $localStorage.currentUser?true:false;

  }

  this.getUser=function(){
    return $localStorage.currentUser.username;
  }

  this.getUsers=function(){
    return $localStorage.users;
  }

  this.saveUser=function(index,user){
    var users = $localStorage.users;
    users[index]=user;
    $localStorage.users=users;
    return users;
  }

  this.removeUser=function(index){
    var users = $localStorage.users;
    users.splice(index,1)
    $localStorage=users;
    return users;
  }

  this.logout=function(){
    delete $localStorage.currentUser;
    return;
  }
});
