var app = angular.module('comicSite');

app.service('ComicService',function($http,$localStorage){
  //var comics=[]

  this.getComics=function(){
    return $localStorage.comics;

  }

  this.setComics=function(data){
    $localStorage.comics=data;
  }

  this.getComic=function(id){

    var comics = $localStorage.comics;
    for (var i = 0; i < comics.length; i++) {
      if(comics[i].id==parseInt(id)){
        return comics[i];
      }
    }
    //return null;
  }

  this.createComic=function(input){
    var newId=0;
    for (var i = 0; i < $localStorage.comics.length; i++) {
      if($localStorage.comics[i].id>newId){
        newId=$localStorage.comics[i].id;
      }
    }
    newId=newId++;
    var comic={
      "id":newId,
      "title" : input.title,
      "characters" : input.characters.split(","),
      "publisher" : input.publisher,
      "date" : parseInt(input.year),
      "number" : parseInt(input.number),
      "specialEdition" : input.special,
      "genre" : input.genre,
      "coverImage" : input.coverImage,
      "borrowed" : false,
      "popularityRank" : parseInt(input.popularityRank),
      "starsAverage" : 0,
      "starsAmount": 0,
      "searchRank" : 0,
      "recomendationRank" : true,
      "timesBorrowed" : 0,
      "comments":[],
      "games" : [],
      "videos" : []
    };

    console.log(comic);

    var comics=$localStorage.comics;
    comics.push(comic);
    $localStorage.comics=comics;

    return comics;
  }

  this.removeComic=function(comic){
    var comics=$localStorage.comics;
    for (var i = 0; i < comics.length; i++) {
      if(comic.title==comics[i].title && comic.coverImage==comics[i].coverImage){
        comics.splice(i,1);
        alert("Comic Deleted");
        break;
      }
    }
    $localStorage.comics=comics;
    return comics;
  }

  this.getCharacters=function(){
    var output=[];
    comics=$localStorage.comics;
    for (var i = 0; i < comics.length; i++) {
      for (var j = 0; j < comics[i].characters.length; j++) {
        if(output.indexOf(comics[i].characters[j])<0){
          output.push(comics[i].characters[j]);
        }
      }
    }
    //console.log(output)
    return output;
  }

  this.getPublishers=function(){
    var output=[];
    comics=$localStorage.comics;
    for (var i = 0; i < comics.length; i++) {
        if(output.indexOf(comics[i].publisher)<0){
          output.push(comics[i].publisher);
        }
    }
    //console.log(output)
    return output;
  }

  this.getGenres=function(){
    var output=[];
    comics=$localStorage.comics;
    for (var i = 0; i < comics.length; i++) {
        if(output.indexOf(comics[i].genre)<0){
          output.push(comics[i].genre);
        }
    }
  //  console.log(output)
    return output;
  }

});
