var app = angular.module('comicSite');

app.controller('MainController', function($scope,LoginService,$location){
  var store= this;// this.product= gems;
  store.title="Comic Store ";
  store.user={};
  $scope.tab={
    genres:false,
    editions:false,
    news:false,
    characters:false,
    profile:false,
  };

  if(!LoginService.checkAuth()){
    $location.path("/login");
  }

  $scope.logout=function(){
    LoginService.logout();
    $location.path("/login");
  }
  $scope.user=LoginService.getUser();

//patern user : ng-pattern="/^[a-zA-Z0-9]*$/"
//Fede : c130710a
//11 opcional

/*
  pendings
  move to directives,
*/

});
