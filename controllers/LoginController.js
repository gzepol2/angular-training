var app = angular.module('comicSite');

app.controller('LoginController',function($scope,LoginService,$location){
  this.user={
    new:false,
    username:"",
    password:""
  };
  this.message="";
  var response={};

  this.catchform=function(user){
    //console.log("formmm",user);
    if(user.new){//register
      response=LoginService.register(user);
    }else{//login
      response=LoginService.login(user);
    }

    if(response.success){
      if(user.username=="admin"){
          $location.path("/admin");
      }else{
        $location.path("/");
      }

    }else{
      this.message=response.message;
    }

  }

  LoginService.init();

});
