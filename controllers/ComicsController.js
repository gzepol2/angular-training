var app = angular.module('comicSite');

app.controller('ComicsController', function($scope,$http,ComicService){
  $scope.comics=[];



  if(typeof(ComicService.getComics())=="undefined"){
      $http.get('resources/comics.json').success(function(data){
      $scope.comics= data;
      ComicService.setComics(data);
      $scope.allGeneres=ComicService.getGenres();
      $scope.generes = [];
      $scope.publishers=ComicService.getPublishers();
      $scope.allCharacters=ComicService.getCharacters();
      $scope.characters=[];
    }).error(function(e){
      console.log(e);
    });
  }else{
    $scope.comics = ComicService.getComics();
    $scope.allGeneres=ComicService.getGenres();
    $scope.generes = [];
    $scope.publishers=ComicService.getPublishers();
    $scope.allCharacters=ComicService.getCharacters();
    $scope.characters=[];
  }



});
