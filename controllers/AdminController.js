var app = angular.module('comicSite');

app.controller("AdminController",function($scope,ComicService,LoginService,$location){

  if(!LoginService.checkAuth()){
    $location.path("/login");
  }else{
    if(LoginService.getUser()!="admin"){
      $location.path("/");
      return;
    }
  }

  $scope.tab={
    comics:true,
    users:false
  }
  $scope.comics=[];
  $scope.comic={};
  $scope.message="";
  $scope.users=[];

  $scope.switchTab=function(tab){
    $scope.tab.comics= tab;
    $scope.tab.users= !tab;
  };

  $scope.createComic=function(comic){
      //console.log(comic);
      $scope.comics=ComicService.createComic(comic);
      $scope.comic={};
      $scope.message="Comic Created!";
  };

  $scope.removeComic=function(comic){
    alert("Remove Comic!");//replace with popup
    $scope.comics=ComicService.removeComic(comic);
  }

  $scope.saveUser=function(index,user){
    console.log(index,user);
    $scope.users=LoginService.saveUser(index,user);
    alert("User Saved");
  }

  $scope.removeUser=function(index){
    console.log(index);
    $scope.users=LoginService.removeUser(index);
    alert("User removed");
  }

  $scope.logout=function(){
    LoginService.logout();
    $location.path("/login");
  }


  if(typeof(ComicService.getComics())=="undefined"){
      $http.get('resources/comics.json').success(function(data){
      $scope.comics= data;
      ComicService.setComics(data);
    }).error(function(e){
      console.log(e);
    });
  }else{
    $scope.comics = ComicService.getComics();
  }

  if(typeof(LoginService.getUsers())!="undefined"){
    $scope.users=LoginService.getUsers();
  }



});
