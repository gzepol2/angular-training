var app = angular.module('comicSite');

app.controller('DetailController',function($scope,$routeParams,ComicService,LoginService){
  $scope.comic=ComicService.getComic($routeParams.id);
  //console.log("comic",$scope.comic);
  this.review={};
  this.getCharacters=function(input){
    return input.join();
  }

  $scope.comic.searchRank=$scope.comic.searchRank+1;

  this.addReview=function(review){
    $scope.comic.comments.push({comment:review.comment,user:LoginService.getUser()});
    $scope.comic.starsAmount=$scope.comic.starsAmount+1;
    $scope.comic.starsAverage=((($scope.comic.starsAverage*($scope.comic.starsAmount-1))+review.stars)/$scope.comic.starsAmount);
  }

  this.borrow=function(){
    console.log("borrow");
    $scope.comic.borrowed=LoginService.getUser();
  }
  this.return=function(){
    $scope.comic.borrowed=false;
  }

  this.canReturn=function(comic){
    return comic.borrowed==LoginService.getUser();
  }

});
