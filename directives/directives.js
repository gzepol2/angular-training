var app = angular.module('comicSite');

app.directive("carousel", function() {
   return {
     restrict: 'E',
     templateUrl: "directives/carousel.html"
   };
 })
 .directive('navbar',function(){
   return {
     restrict: 'E',
     templateUrl: "directives/navbar.html"
   };
 })
 .directive('sidebar',function(){
   return {
     restrict: 'E',
     templateUrl: "directives/sidebar.html"
   };
 })
 .directive('profileDetail',function(){
   return {
     restrict: 'E',
     templateUrl: "directives/profile-detail.html"
   };
 })
 .directive('panelFilter',function(){
   return {
     restrict: 'E',
     templateUrl: "directives/panel-filter.html"
   };
 })
 .directive('comicsContainer',function(){
   return {
     restrict: 'E',
     templateUrl: "directives/comics-container.html"
   };
 })
